virt service demo

```
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')
export SECURE_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="https")].nodePort}')
export TCP_INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="tcp")].nodePort}')
export INGRESS_HOST=$(kubectl get po -l istio=ingressgateway -n istio-system -o jsonpath='{.items[0].status.hostIP}')

curl  -L -s -I -HHost:example.com "http://$INGRESS_HOST:$INGRESS_PORT/"

```

mtls test

```
k edit ns # remove if there is 
k run nginx --image nginx
k run nginx2 --image nginx
k edit ns # add the istio flag back

k run nginx-side --image nginx
k run nginx-side2 --image nginx

k expose nginx --port 80
k expose nginx2 --port 80
k expose nginx-side --port 80
k expose nginx-side2 --port 80


# select the STRICT lockdown
k exec -it nginx -- curl nginx2
k exec -it nginx -- curl nginx-side
k exec -it nginx-side -- curl nginx-side2
k exec -it nginx-side -- curl nginx

```
