# install wp

```
kubectl label namespace default istio-injection=enabled --overwrite

mkdir ~/wd
cd ~/wd
curl -LO https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
curl -LO https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml
cat <<EOF >>./kustomization.yaml
resources:
  - mysql-deployment.yaml
  - wordpress-deployment.yaml
  - pvs.yaml
EOF

cat << EOF > pvs.yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: task-pv-volume-1
  labels:
    type: local
spec:
  capacity:
    storage: 20Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/tmp/data1"
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: task-pv-volume-2
  labels:
    type: local
spec:
  capacity:
    storage: 20Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/tmp/data2"
EOF

k apply -k .
```

config gateway and virt-service

```
kubectl apply -f - <<EOF
apiVersion: networking.istio.io/v1beta1
kind: VirtualService
metadata:
  name: wp-virt-service
  namespace: default
spec:
  gateways:
  - wp-gateway
  hosts:
  - example.com
  http:
  - match:
    - uri:
        prefix: /
    route:
    - destination:
        host: wpi
        port:
          number: 80
---
apiVersion: networking.istio.io/v1beta1
kind: Gateway
metadata:
  name: wp-gateway
  namespace: default
spec:
  selector:
    istio: ingressgateway
  servers:
  - hosts:
    - example.com
    port:
      name: http
      number: 80
      protocol: HTTP
EOF
```

lock down mtls if that is needed
```
kubectl apply -n foo -f - <<EOF
apiVersion: security.istio.io/v1beta1
kind: PeerAuthentication
metadata:
  name: "default"
spec:
  mtls:
    mode: STRICT
EOF
```


